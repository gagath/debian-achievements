// SPDX-FileCopyrightText: 2022 Agathe Porte <microjoe@microjoe.org>
//
// SPDX-License-Identifier: Apache-2.0 OR MIT

use sqlx::Executor;
use sqlx::Postgres;

// Struct definition

pub struct Counter<'a> {
    pub title: &'a str,
    pub query: &'a str,
}

impl<'a> Counter<'a> {
    pub async fn execute<'p, E>(
        &self,
        executor: E,
        maintainer_email: &str,
    ) -> Result<(&Counter<'a>, i64), sqlx::Error>
    where
        E: Executor<'p, Database = Postgres>,
    {
        Ok((
            self,
            sqlx::query_as::<_, (i64,)>(self.query)
                .bind(maintainer_email)
                .fetch_one(executor)
                .await?
                .0,
        ))
    }
}

// Macro definition

macro_rules! counters {
    ($($name:ident, $($query:literal)+);+) => {
        $(
            pub static $name: Counter = Counter {
                title: stringify!($name),
                query: concat!($($query, " "),+),
            };
        )+

        pub static COUNTERS: &[&Counter] = &[
            $(&$name),+
        ];
    };
}

// Counters

counters!(
    BUG,
    "SELECT COUNT(*) FROM bugs"
    "WHERE submitter_email = $1"
    ;

    ITP,
    "SELECT COUNT(*) FROM bugs"
    "WHERE package = 'wnpp'"
    "AND title LIKE 'ITP:%'"
    "AND submitter_email = $1"
    ;

    RFA,
    "SELECT COUNT(*) FROM bugs"
    "WHERE package = 'wnpp'"
    "AND title LIKE 'RFA:%'"
    "AND submitter_email = $1"
);
