// SPDX-FileCopyrightText: 2022 Agathe Porte <microjoe@microjoe.org>
//
// SPDX-License-Identifier: Apache-2.0 OR MIT

use sqlx::Executor;
use sqlx::Postgres;

use crate::counters::*;

// Struct definition

pub struct Achievement<'a> {
    pub title: &'a str,
    pub description: &'a str,
    pub counter: &'a Counter<'a>,
    pub success: fn(i64) -> bool,
}

impl<'a> Achievement<'a> {
    pub async fn is_completed<'p, E>(
        &self,
        executor: E,
        maintainer_email: &str,
    ) -> Result<(&Achievement<'a>, bool), sqlx::Error>
    where
        E: Executor<'p, Database = Postgres>,
    {
        let (_, res) = self.counter.execute(executor, maintainer_email).await?;
        Ok((self, (self.success)(res)))
    }
}

// Macro definition

macro_rules! achievements {
    ($($name:ident, $title:expr, $description:expr, $counter:expr, $success:tt);+) => {
        $(
            pub static $name: Achievement = Achievement {
                title: $title,
                description: $description,
                counter: &$counter,
                success: $success,
            };
        )+

        pub static ACHIEVEMENTS: &[&Achievement] = &[
            $(&$name),+
        ];
    };
}

// Achievements

achievements!(
    FIRST_BUG,
    "First bug",
    "Create your first bug",
    BUG,
    (|c| c >= 1)
    ;

    BUG_HUNTER,
    "Bug hunter",
    "Create 5 bugs",
    BUG,
    (|c| c >= 5)
    ;

    MONSTER_BUG,
    "Monster bug",
    "Create 100 bugs",
    BUG,
    (|c| c >= 100)
);
