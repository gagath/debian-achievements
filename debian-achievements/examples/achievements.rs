// SPDX-FileCopyrightText: 2022 Agathe Porte <microjoe@microjoe.org>
//
// SPDX-License-Identifier: Apache-2.0 OR MIT

use futures::future::join_all;
use futures::TryFutureExt;

use sqlx::postgres::PgPoolOptions;

use debian_achievements::achievements::ACHIEVEMENTS;

use clap::Parser;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Email address of the maintainer to display the counters for
    maintainer_email: String,

    /// Use local database instead of udd-mirror
    #[clap(short, long)]
    local: bool,
}

#[async_std::main]
async fn main() -> Result<(), sqlx::Error> {
    env_logger::init();

    let args = Args::parse();

    let db = if args.local {
        "postgres://udd:udd@localhost/udd"
    } else {
        "postgresql://udd-mirror:udd-mirror@udd-mirror.debian.net/udd"
    };

    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(db)
        //.connect("postgresql://udd-mirror:udd-mirror@udd-mirror.debian.net/udd")
        .await?;

    let mut j = vec![];
    for c in ACHIEVEMENTS {
        j.push(
            c.is_completed(&pool, "debian@microjoe.org")
                .and_then(|(a, res)| async move {
                    if res {
                        println!("{} {}", a.title, a.description);
                    }
                    Ok(())
                }),
        );
    }

    join_all(j).await;

    Ok(())
}
